# -*- coding: utf-8 -*-
#
#    Copyright (C) 17/12/2014  Jose J. Duran, cavefish@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from openerp import models, fields
import logging
log = logging.getLogger(__name__)


class res_users(models.Model):
    _inherit = 'res.users'

    def _auth_oauth_validate(self, cr, uid, provider, access_token, context=None):
        ret = super(res_users, self)._auth_oauth_validate(cr, uid, provider, access_token, context=context)
        if not ret.get('user_id') and ret.get('id'):
            ret['user_id'] = ret['id']
        return ret
